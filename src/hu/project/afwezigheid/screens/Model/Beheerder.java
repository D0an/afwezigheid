package hu.project.afwezigheid.screens.Model;

import hu.project.afwezigheid.Main;

import java.util.ArrayList;
import java.util.List;

public class Beheerder extends Persoon {

    private static List<Beheerder> allAdmins = new ArrayList<>();

    public Beheerder(String name, String email, String password) {
        super(name, email, password);
    }

    public static void addStudent(Leerling student) {
        if (alreadyExists(student)) return;
        Leerling.getAllStudents().add(student);
        Main.getDatabaseManager().saveStudent(student);
    }

    public static void addSlber(Slber slber) {
        if (alreadyExists(slber)) return;
        Slber.getAllSlber().add(slber);
        Main.getDatabaseManager().saveSlber(slber);
    }

    public static void addTeacher(Docent teacher) {
        if (alreadyExists(teacher)) return;
        Docent.getAllTeachers().add(teacher);
        Main.getDatabaseManager().saveTeacher(teacher);
    }

    public static void addAdmin(Beheerder admin) {
        if (alreadyExists(admin)) return;
        getAllAdmins().add(admin);
        Main.getDatabaseManager().saveAdmin(admin);
    }

    public static boolean alreadyExists(Persoon person) {
        return getPersonFromEmail(person.getEmail()) != null;
    }

    public void removePerson(Persoon person) {
        if (person instanceof Leerling && Leerling.getAllStudents().contains(person)) {
            Leerling.getAllStudents().remove(person);
        }
        else if (person instanceof Slber && Slber.getAllSlber().contains(person)) {
            Slber.getAllSlber().remove(person);
        }
        else if (person instanceof Docent && Docent.getAllTeachers().contains(person)) {
            Docent.getAllTeachers().remove(person);
        }
        else if (getAllAdmins().contains(person)){
            getAllAdmins().remove(person);
        }
    }

    public static List<Beheerder> getAllAdmins() {
        return allAdmins;
    }

    public static List<Persoon> getAllPeople() {
        List<Persoon> personList = new ArrayList<>();
        personList.addAll(Slber.getAllSlber());
        personList.addAll(Beheerder.getAllAdmins());
        personList.addAll(Leerling.getAllStudents());
        personList.addAll(Docent.getAllTeachers());

        return personList;
    }

    public static Persoon getPersonFromEmail(String email) {
        return getAllPeople().stream().filter(all -> all.getEmail().equalsIgnoreCase(email)).findFirst().orElse(null);
    }

    public void save() {
        Main.getDatabaseManager().saveAdmin(this);
    }

    @Override
    public String toString() {
        return getName();
    }
}
