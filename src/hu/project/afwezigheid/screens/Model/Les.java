package hu.project.afwezigheid.screens.Model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.UUID;

public class Les {

    private UUID uniqueId;

    private LocalDateTime start;
    private LocalDateTime end;
    private String locatie;
    private ArrayList<Leerling> attendants = new ArrayList<>();
    private Klas classGivingTo;
    private Docent classGivenBy;
    private ArrayList<Leerling> absentees = new ArrayList<>();

    public Les(UUID uuid, LocalDateTime start, LocalDateTime end, Klas klas, Docent docent) {
        this.uniqueId = uuid;

        this.start = start;
        this.end = end;
        this.classGivingTo = klas;
        this.classGivenBy = docent;

        this.classGivingTo.addLesson(this);
        this.classGivenBy.addLesson(this);
        this.locatie = "thuis vanwege Covid-19";
    }

    public LocalDateTime getStart() {
        return start;
    }

    public LocalDateTime getEnd() {return end;}

    public Klas getClassGivingTo() {
        return classGivingTo;
    }

    public void addAbsence(Afwezigheid abscense){
        absentees.add(abscense.getAbsentPupil());
    }

    public void presentAnyway(Leerling leerling){
        attendants.add(leerling);
        absentees.remove(leerling);
    }

    public UUID getUniqueId(){
        return uniqueId;
    }

    public void AbsentAnyway(Leerling leerling){
        attendants.remove(leerling);
        absentees.add(leerling);
    }

    public ArrayList<Leerling> getAttendants() {
        return attendants;
    }

    public ArrayList<Leerling> getAbsentees() {
        return absentees;
    }

    @Override
    public String toString() {
        return "start om: " + start.format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)) +
                "\nen eindigt om: " + end.toLocalTime().format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT)) +
                "\nlocatie: " + locatie;
    }
}
