package hu.project.afwezigheid.screens.Model;

import java.util.Objects;

public class Persoon {
    private String name;
    private String email;
    private String password;

    public Persoon(String name, String email, String password) {
        this.name = name;
        this.email = email;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public boolean matchestPassword(String password) {
        return password.equals(password);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Persoon persoon = (Persoon) o;
        return Objects.equals(email, persoon.email);
    }

    public String getPassword() {
        return password;
    }
}
