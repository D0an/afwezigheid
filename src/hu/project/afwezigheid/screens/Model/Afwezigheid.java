package hu.project.afwezigheid.screens.Model;

public abstract class Afwezigheid {
    private String reason;
    private Leerling absentPupil;


    public Afwezigheid(Leerling lr, String reason) {
        this.reason = reason;
        this.absentPupil = lr;
        this.absentPupil.addAbsent(this);
    }

    public String getReason() {
        return this.reason;
    }

    public Leerling getAbsentPupil() {
        return absentPupil;
    }
}
