package hu.project.afwezigheid.screens.Model;

import java.util.ArrayList;
import java.util.Objects;
import java.util.UUID;

public class Klas {
    private String klasnr;
    private ArrayList<Leerling> students = new ArrayList<>();
    private ArrayList<Les> lessons = new ArrayList<>();
    private static ArrayList<Klas> allClasses = new ArrayList<>();


    public Klas(String klasnr) {
        this.klasnr = klasnr;
        allClasses.add(this);
    }

    public void addStudent(Leerling student) {
        students.add(student);
        // add class? ipv set
        student.setMyClass(this);
    }

    public void addStudents(Leerling... s) {
        for(Leerling student : s){
            addStudent(student);
        }
        // add class? ipv set

    }

    public Les getLesson(String uuid) {

        return getLessons().stream().filter(les -> {

            System.out.println("Dab " + les.getUniqueId().toString());
            return les.getUniqueId().toString().equalsIgnoreCase(uuid);
        }).findAny().orElse(null);
    }

    public void removeStudent(Leerling student) {
        students.remove(student);
        // remove class? ipv set
        student.setMyClass(null);
    }

    public ArrayList<Les> getLessons() {
        return lessons;
    }

    public void addLesson(Les lesson) {
        lessons.add(lesson);
    }

    public ArrayList<Leerling> getStudents() {
        return students;
    }

    public static ArrayList<Klas> getAllClasses() {
        return allClasses;
    }

    public static Klas getClassFromClassNumber(String klasnr) {
        return getAllClasses().stream().filter(klas -> klas.toString().equals(klasnr)).findAny().orElse(null);
    }

    @Override
    public String toString() {
        return klasnr;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Klas klas = (Klas) o;
        return klasnr.equals(klas.klasnr);
    }
}
