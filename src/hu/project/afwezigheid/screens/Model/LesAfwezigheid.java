package hu.project.afwezigheid.screens.Model;

public class LesAfwezigheid extends Afwezigheid {

    private Les lesson;

    public LesAfwezigheid(Leerling lr, Les lesson, String reason) {
        super(lr, reason);
        this.lesson = lesson;
    }

    public Les getLesson() {
        return lesson;
    }

    public void setLesson(Les lesson) {
        this.lesson = lesson;
    }

    @Override
    public String toString() {
        return "afwezig bij: " + lesson.toString() + "\nomdat: " + getReason();
    }
}
