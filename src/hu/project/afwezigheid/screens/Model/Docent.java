package hu.project.afwezigheid.screens.Model;

import hu.project.afwezigheid.Main;

import java.util.ArrayList;

public class Docent extends Persoon {
    private ArrayList<Les> lessons = new ArrayList<>();
    private static ArrayList<Docent> allTeachers = new ArrayList<>();

    public Docent(String name, String email, String password) {
        super(name, email, password);
    }

    public void addLesson(Les lesson) {
        lessons.add(lesson);
    }

    public ArrayList<Les> getLessons() {
        return lessons;
    }

    public static ArrayList<Docent> getAllTeachers() {
        return allTeachers;
    }

    public void save() {
        Main.getDatabaseManager().saveTeacher(this);
    }

    @Override
    public String toString() {
        return getName();
    }
}
