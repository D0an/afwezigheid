package hu.project.afwezigheid.screens.Model;

import hu.project.afwezigheid.Main;

import java.util.ArrayList;

public class Slber extends Persoon{
    private ArrayList<Leerling> students = new ArrayList<>();
    private static ArrayList<Slber> allSlber = new ArrayList<>();

    public Slber(String name, String email, String password) {
        super(name, email, password);
    }

    public void addStudent(Leerling student) {
        students.add(student);
    }

    public void removeStudent(Leerling pupil) {
        students.remove(pupil);
    }

    public ArrayList<Leerling> getStudents() {
        return students;
    }

    public static ArrayList<Slber> getAllSlber() {
        return allSlber;
    }

    public void save() {
        Main.getDatabaseManager().saveSlber(this);
    }

    @Override
    public String toString() {
        return this.getName();
    }
}