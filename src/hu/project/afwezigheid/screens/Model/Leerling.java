package hu.project.afwezigheid.screens.Model;

import hu.project.afwezigheid.Main;

import java.util.ArrayList;
import java.util.List;

public class Leerling extends Persoon {
    private Slber mentor;
    private Klas myClass;
    private List<Afwezigheid> myAbsents = new ArrayList<>();

    private static List<Leerling> allStudents = new ArrayList<>();

    public Leerling(String name, String email, String password, Slber mentor) {
        super(name, email, password);
        this.mentor = mentor;
        this.mentor.addStudent(this);
    }

    public Klas getMyClass() {
        return myClass;
    }

    public void save() {
        Main.getDatabaseManager().saveStudent(this);
    }

    public void setMyClass(Klas myClass) {
        this.myClass = myClass;
    }

    public void addAbsent(Afwezigheid absent) {
        myAbsents.add(absent);
    }

    public List<Afwezigheid> getMyAbsents() {
        return myAbsents;
    }

    public int getTotalAbsents() {
        int total = 0;
        for (Afwezigheid absent: myAbsents
             ) {
            if (absent instanceof DatumsAfwezigheid) {
                total += ((DatumsAfwezigheid) absent).getTotalLessons();
            } else {
                total += 1;
            }
        }
        return total;
    }

    @Override
    public String toString() {
        return getName();
    }

    public Slber getMentor() {
        return mentor;
    }

    public static List<Leerling> getAllStudents() {
        return allStudents;
    }
}