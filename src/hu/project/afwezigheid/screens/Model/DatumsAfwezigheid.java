package hu.project.afwezigheid.screens.Model;

import hu.project.afwezigheid.Main;

import java.time.LocalDate;
import java.util.ArrayList;

public class DatumsAfwezigheid extends Afwezigheid {

    private LocalDate beginDate;
    private LocalDate endDate;
    private ArrayList<Les> lessons = new ArrayList<>();

    public DatumsAfwezigheid(Leerling student, String reason, LocalDate beginDate, LocalDate endDate){
        super(student, reason);
        this.beginDate = beginDate;
        this.endDate = endDate;


        Klas userInThisClass = getAbsentPupil().getMyClass();
        for (Les lesson: userInThisClass.getLessons()) {
            // is date between dates
            if ((lesson.getEnd().toLocalDate().isAfter(this.beginDate)
                    || lesson.getEnd().toLocalDate().isEqual(this.beginDate))
                    && (lesson.getStart().toLocalDate().isBefore(this.endDate)
                    || lesson.getStart().toLocalDate().isEqual(this.endDate))
            ) {
                // todo periode afmelden?
                lesson.addAbsence(this);
                lessons.add(lesson);
            }
        }
    }

    public LocalDate getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(LocalDate beginDate) {
        this.beginDate = beginDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public int getTotalLessons() {
        return lessons.size();
    }

    public ArrayList<Les> getLessons() {
        return lessons;
    }

    @Override
    public String toString() {
        return "Afwezig van: " + beginDate +
                "\ntot: " + endDate +
                "\ndagen absent: " +  beginDate.datesUntil(endDate).count() +
                "\nlessen gemist: " + lessons.size() +
                "\nvanwege: " + getReason();
    }
}
