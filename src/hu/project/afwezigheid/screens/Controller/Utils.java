package hu.project.afwezigheid.screens.Controller;

import hu.project.afwezigheid.Main;
import hu.project.afwezigheid.screens.Model.Afwezigheid;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Pair;

import java.util.ArrayList;

public class Utils {
    public static ObservableList<String> giveReasonsToAbsence() {
        return FXCollections.observableArrayList("ziek", "te laat", "medisch", "overig");
    }

    public static void openScreen(String file, String title) {
        try {
            String fxmlPagina = file;
            FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource(fxmlPagina));
            Parent root = fxmlLoader.load();
            Stage stage = new Stage();

            stage.setTitle(title);
            stage.setScene(new Scene(root));
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();
        } catch (Exception e){
            e.getMessage();
            e.printStackTrace();
        }
    }

    public static void closeScreen(){
        Stage.getWindows().get(Stage.getWindows().size()-1).hide();
    }

    public static void updateValue(ArrayList<Pair<String, Integer>> pairs, Afwezigheid absent, int value){
        if (Utils.giveReasonsToAbsence().contains(absent.getReason())) {
            for (int i = 0; i < pairs.size(); i++) {
                if (pairs.get(i).getKey().equals(absent.getReason())) {
                    pairs.set(i , new Pair(absent.getReason(), pairs.get(i).getValue() + value));
                    break;
                }
            }
        } else {
            for (int i = 0; i < pairs.size(); i++) {
                if (pairs.get(i).getKey().equals("overig")) {
                    pairs.set(i , new Pair("overig", pairs.get(i).getValue() + value));
                    break;
                }
            }
        }
    }
}
