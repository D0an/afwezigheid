package hu.project.afwezigheid.screens.Controller;

import hu.project.afwezigheid.Main;
import hu.project.afwezigheid.screens.Model.Afwezigheid;
import hu.project.afwezigheid.screens.Model.Leerling;
import hu.project.afwezigheid.screens.Model.Les;
import hu.project.afwezigheid.screens.Model.LesAfwezigheid;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

public class AfmeldschermLeerlingController implements Initializable {

    public DatePicker afwezigheidDatePicker;
    public ComboBox vakAfwezig;
    public TextArea overigTextArea;
    public ComboBox reasonBox;

    // selecteer de datum als student
    public void selectDatum(ActionEvent actionEvent) {
        vakAfwezig.setItems(null);
        ObservableList<Les> lessons = FXCollections.observableArrayList();
        Leerling currentUser = (Leerling) Main.getCurrentUser();
        for (Les lesson: currentUser.getMyClass().getLessons()
             ) {
            if (lesson.getStart().toLocalDate().isEqual(afwezigheidDatePicker.getValue())) {
                lessons.add(lesson);
            }
        }
        vakAfwezig.setItems(lessons);
        vakAfwezig.getSelectionModel().selectFirst();
    }

    // Laat alle vakken zien die op die datum zijn
    public void showVakkenToday(ActionEvent actionEvent) {
        //todo deze is niet nodig
    }


    // Sluit het scherm, nadat een controle is of er wel een reden is ingevuld
    public void closeScherm(ActionEvent actionEvent) {
        if (vakAfwezig.getValue() != null && afwezigheidDatePicker != null &&
                reasonBox.getValue().equals("overig") ?
                !overigTextArea.getText().isBlank() : true) {
            new LesAfwezigheid((Leerling) Main.getCurrentUser(), (Les) vakAfwezig.getValue(), reasonBox.getValue().equals("overig") ?
                    overigTextArea.getText() : reasonBox.getValue().toString());

            ((Leerling)Main.getCurrentUser()).save();
            Utils.closeScreen();
        }
    }

    // terug naar menu btn
    public void backToMenu(ActionEvent actionEvent) {
        Stage.getWindows().get(Stage.getWindows().size()-1).hide();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        afwezigheidDatePicker.setValue(LocalDate.now());
        // https://stackoverflow.com/questions/48238855/how-to-disable-past-dates-in-datepicker-of-javafx-scene-builder
        afwezigheidDatePicker.setDayCellFactory(picker -> new DateCell() {
            public void updateItem(LocalDate date, boolean empty) {
                super.updateItem(date, empty);
                LocalDate today = LocalDate.now();

                setDisable(empty || date.compareTo(today) < 0 );
            }
        });
        //
        reasonBox.setItems(Utils.giveReasonsToAbsence());
        reasonBox.getSelectionModel().selectFirst();
        selectDatum(null);

    }

    public void valueHasBeenUpdated(ActionEvent actionEvent) {
        if (reasonBox.getValue().equals("overig")) {
            overigTextArea.setEditable(true);
        } else {
            overigTextArea.setEditable(false);
            overigTextArea.setText(null);
        }

    }
}
