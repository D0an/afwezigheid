package hu.project.afwezigheid.screens.Controller;

import hu.project.afwezigheid.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class HomescreenBeheerderController {
    public void logUit(ActionEvent actionEvent) {
        Utils.closeScreen();
    }

    public void creeerLeerling(ActionEvent actionEvent) {
        /*
        TODO:
        Button werkt alleen als er al een SLBer is.
        (Kan alleen een leerling aanmaken als er al een SLBer bestaat!)
         */
        Utils.openScreen("screens/View/Beheerder/CreeerLeerlingBeheerder.fxml", "Creëer leerling");
    }

    public void creeerDocent(ActionEvent actionEvent) {
        Utils.openScreen("screens/View/Beheerder/CreeerDocentBeheerder.fxml", "Creëer docent");
    }

    public void creeerSlber(ActionEvent actionEvent) {
        Utils.openScreen("screens/View/Beheerder/CreeerSlberBeheerder.fxml", "Creëer Slber");
    }
}
