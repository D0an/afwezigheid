package hu.project.afwezigheid.screens.Controller;

import hu.project.afwezigheid.screens.Model.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Label;
import javafx.util.Pair;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class LeerlingProfileSlbController implements Initializable {
    public PieChart aanwezigheidChart;
    public Label name;

    public void terug(ActionEvent actionEvent) {
        Utils.closeScreen();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        Leerling currentPupil = AfwezigheidTonenSlbController.getSelectedPupil();
        name.setText(currentPupil.getName());
        ArrayList<Pair<String, Integer>> pairs = new ArrayList<>();
        for (String s: Utils.giveReasonsToAbsence()) {
            pairs.add(new Pair(s, 0));
        }
        pairs.add(new Pair("present", 0));

        ArrayList<Les> alreadyCheckedThisObject = new ArrayList<>();
        for (Les lesson:currentPupil.getMyClass().getLessons()) {
            if (lesson.getAbsentees().contains(currentPupil)) {
                for (Afwezigheid absent: currentPupil.getMyAbsents()) {
                    if (absent instanceof LesAfwezigheid && ((LesAfwezigheid) absent).getLesson().equals(lesson)) {
                        Utils.updateValue(pairs, absent, 1);
                    } else if (absent instanceof DatumsAfwezigheid){
                        boolean lessonIsCheckedBefore = false;
                        for (Les lessonInAbsentInstance: ((DatumsAfwezigheid) absent).getLessons()) {
                            if (lesson.equals(lessonInAbsentInstance) && !alreadyCheckedThisObject.contains(lesson)) lessonIsCheckedBefore = true;
                        }
                        if (lessonIsCheckedBefore) {
                            for (Les lessonInAbsentInstance: ((DatumsAfwezigheid) absent).getLessons()) {
                                alreadyCheckedThisObject.add(lessonInAbsentInstance);
                            }
                            Utils.updateValue(pairs, absent, ((DatumsAfwezigheid) absent).getTotalLessons());
                        }
                    }
                }
            } else {
                for (int i = 0; i < pairs.size(); i++) {
                    if (pairs.get(i).getKey().equals("present")) {
                        pairs.set(i , new Pair("present", pairs.get(i).getValue() + 1));
                        break;
                    }
                }
            }
        }



        ObservableList<PieChart.Data> data = FXCollections.observableArrayList();

        for (int i = 0; i < pairs.size(); i++) {
            data.add(new PieChart.Data(pairs.get(i).getKey(), pairs.get(i).getValue()));
        }


        aanwezigheidChart.setData(data);
        aanwezigheidChart.setTitle("aanwezigheid");
        System.out.println(aanwezigheidChart.getData());
    }
}
