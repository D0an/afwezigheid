package hu.project.afwezigheid.screens.Controller;

import hu.project.afwezigheid.Main;
import hu.project.afwezigheid.screens.Model.*;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

public class LangdurigAfmeldenController implements Initializable {
    public DatePicker beginDatum;
    public DatePicker eindDatum;

    public TextArea overigTextArea;
    public ComboBox reasonBox;

    public void selectBeginDate(ActionEvent actionEvent) {
    }

    public void selectEndDate(ActionEvent actionEvent) {
    }

    public void meldAfwezig(ActionEvent actionEvent) {
        if (beginDatum.getValue() != null &&
                eindDatum.getValue() != null &&
                reasonBox.getValue().equals("overig") ?
                !overigTextArea.getText().isBlank() : true) {
            Leerling user = (Leerling) Main.getCurrentUser();
            Klas userInThisClass = user.getMyClass();
            new DatumsAfwezigheid(user, reasonBox.getValue().equals("overig") ? overigTextArea.getText() : reasonBox.getValue().toString(), beginDatum.getValue(), eindDatum.getValue());
            user.save();
            Utils.closeScreen();
        }
    }

    public void backToHomescreen(ActionEvent actionEvent) {
        Utils.closeScreen();;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        beginDatum.setValue(LocalDate.now());
        // https://stackoverflow.com/questions/48238855/how-to-disable-past-dates-in-datepicker-of-javafx-scene-builder
        beginDatum.setDayCellFactory(picker -> new DateCell() {
            public void updateItem(LocalDate date, boolean empty) {
                super.updateItem(date, empty);
                LocalDate today = LocalDate.now();

                setDisable(empty || date.compareTo(today) < 0 );
            }
        });
        eindDatum.setDayCellFactory(picker -> new DateCell() {
            public void updateItem(LocalDate date, boolean empty) {
                super.updateItem(date, empty);
                LocalDate today = LocalDate.now();

                setDisable(empty || date.compareTo(today) < 0 );
            }
        });
        //
        reasonBox.setItems(Utils.giveReasonsToAbsence());
        reasonBox.getSelectionModel().selectFirst();
    }

    public void valueHasBeenUpdated(ActionEvent actionEvent) {
        if (reasonBox.getValue().equals("overig")) {
            overigTextArea.setEditable(true);
        } else {
            overigTextArea.setEditable(false);
            overigTextArea.setText(null);
        }
    }
}
