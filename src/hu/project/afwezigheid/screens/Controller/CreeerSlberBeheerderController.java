package hu.project.afwezigheid.screens.Controller;


import hu.project.afwezigheid.screens.Model.Beheerder;
import hu.project.afwezigheid.screens.Model.Slber;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class CreeerSlberBeheerderController {
    public TextField voornaamField;
    public TextField tussenvoegselField;
    public TextField achternaamField;
    public TextField emailField;
    public TextField wachtwoordField;
    private Alert alert = new Alert(Alert.AlertType.WARNING);

    public void terug(ActionEvent actionEvent) {
        Stage.getWindows().get(Stage.getWindows().size()-1).hide();
    }

    public void createDocent(ActionEvent actionEvent) {
        if (!voornaamField.getText().isBlank()
                && !achternaamField.getText().isBlank()
                && !emailField.getText().isBlank()
                && !wachtwoordField.getText().isBlank()) {
            Slber teacher;
            if (!tussenvoegselField.getText().isBlank()) {
                teacher = new Slber(voornaamField.getText() + " " + tussenvoegselField.getText() + " " + achternaamField.getText(),
                        emailField.getText(), wachtwoordField.getText());
            } else {
                teacher = new Slber(voornaamField.getText() + " " + achternaamField.getText(),
                        emailField.getText(), wachtwoordField.getText());
            }
            Beheerder.addSlber(teacher);
            Utils.closeScreen();;
        } else {
            alert.setTitle("Waarschuwing");
            alert.setHeaderText("Let op.");
            alert.setContentText("U moet alle velden invullen voordat u een SLBer kunt aanmaken.");

            alert.show();
        }
    }
}
