package hu.project.afwezigheid.screens.Controller;

import hu.project.afwezigheid.Main;
import hu.project.afwezigheid.screens.Model.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.stage.Stage;

import java.net.URL;
import java.time.LocalDate;
import java.time.Month;
import java.util.ResourceBundle;

public class AfwezigheidOverzichtLeerlingController implements Initializable {
    public ListView afwezigheidList;
    public DatePicker beginDatum;
    public DatePicker eindDatum;

    /*
    Een Student selecteerd de datums waarvan hij wilt zien dat hij absent was. dus hij kan kiezen voor een lange periode of alleen de dag
     */
    public void updateDates(ActionEvent actionEvent) {
        if (beginDatum.getValue() != null && eindDatum.getValue() != null) {
            ObservableList absents = FXCollections.observableArrayList();
            Leerling llrUser = (Leerling) Main.getCurrentUser();
            for (Afwezigheid absent: llrUser.getMyAbsents()
            ) {
                if (absent instanceof LesAfwezigheid) {
                    // one specific class
                    Les lesson = ((LesAfwezigheid) absent).getLesson();
                    if ((lesson.getEnd().toLocalDate().isAfter(beginDatum.getValue())
                            || lesson.getEnd().toLocalDate().isEqual(beginDatum.getValue()))
                            && (lesson.getStart().toLocalDate().isBefore(eindDatum.getValue())
                            || lesson.getStart().toLocalDate().isEqual(eindDatum.getValue()))
                    ) {
                        absents.add((LesAfwezigheid)absent);
                    }

                } else {
                    // one period absent
                    LocalDate periodBeginDate = ((DatumsAfwezigheid) absent).getBeginDate();
                    LocalDate periodEndDate = ((DatumsAfwezigheid) absent).getEndDate();

                    if ((periodEndDate.isAfter(beginDatum.getValue())
                            || periodEndDate.isEqual(beginDatum.getValue()))
                            && (periodBeginDate.isBefore(eindDatum.getValue())
                            || periodBeginDate.isEqual(eindDatum.getValue()))
                    ) {
                        absents.add((DatumsAfwezigheid)absent);
                    }

                }
            }
            afwezigheidList.setItems(absents);
        }
    }

    public void terug(ActionEvent actionEvent) {
        Utils.closeScreen();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        // set current date to today
        eindDatum.setValue(LocalDate.now());
        if (LocalDate.now().getMonth().compareTo(Month.AUGUST) < 0) {
            // school started in august previous year
            beginDatum.setValue(LocalDate.of(LocalDate.now().minusYears(1).getYear(), Month.AUGUST, 1));
        } else {
            // school started in august this year
            beginDatum.setValue(LocalDate.of(LocalDate.now().getYear(), Month.AUGUST, 1));
        }

        updateDates(null);
    }
}
