package hu.project.afwezigheid.screens.Controller;

import hu.project.afwezigheid.Main;
import hu.project.afwezigheid.screens.Model.*;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class LoginController {

    public PasswordField wachtwoordInput;
    public TextField emailInput;

    public void closeApp(ActionEvent actionEvent) {
        Platform.exit();
    }

    public Persoon validCredentials(String email, String ww){
        //todo database

        System.out.println(Beheerder.getAllPeople());

        Persoon inLoggend = Beheerder.getPersonFromEmail(email);

        if(inLoggend != null) {
            if(inLoggend.matchestPassword(ww)){
                return inLoggend;
            }
        }

        return null;
//        if (email.equals("leerling@hu.nl" ) && ww.equals("password")) {
//            return llr;
//        } else if (email.equals("slb@hu.nl" ) && ww.equals("password")) {
//            return slber;
//        } else if (email.equals("docent@hu.nl" ) && ww.equals("password")) {
//            return testDocent;
//        } else if (email.equals("beheerder@hu.nl" ) && ww.equals("password")) {
//            return admin;
//        } else {
//            return null;
//        }
    }

    public void loginBtn(ActionEvent actionEvent) {
        // check if input is blank otherwise you'll get nullpointer
        if (!(emailInput.getText().isBlank() || wachtwoordInput.getText().isBlank())) {
            // gets a subclass in return if the login information is right, otherwise a null will be returned
            Persoon user = validCredentials(emailInput.getText(), wachtwoordInput.getText());

            if (user != null) {
                Main.setCurrentUser(user);
                Utils.openScreen("screens/View/" + user.getClass().getSimpleName() + "/Homescreen" + user.getClass().getSimpleName() + ".fxml", "Home");
            } else {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Waarschuwing");
                alert.setHeaderText("Het ingevoerde email adres komt niet overeen met het ingevoerde wachtwoord.");
                alert.setContentText("Als dit een fout is moet u contact opnemen met de beheerder. (admin@afwezigheid.hu.nl)");
                alert.show();
            }
        }
        emailInput.setText("");
        wachtwoordInput.setText("");
    }
}
