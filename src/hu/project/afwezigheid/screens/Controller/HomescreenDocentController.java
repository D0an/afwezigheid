package hu.project.afwezigheid.screens.Controller;

import hu.project.afwezigheid.Main;
import hu.project.afwezigheid.screens.Model.Docent;
import hu.project.afwezigheid.screens.Model.Les;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Comparator;
import java.util.ResourceBundle;

public class HomescreenDocentController implements Initializable {

    public ComboBox lesBox;
    private static Les currentLesson;
    public Label todayDate;
    public Label welcomeUser;
    @FXML private CheckBox verleden = new CheckBox();

    public void selectLes(ActionEvent actionEvent) {
        setCurrentLesson((Les) lesBox.getValue());
    }

    public void sluitScherm(ActionEvent actionEvent) {
        Utils.closeScreen();
    }

    public void toonOverzicht(ActionEvent actionEvent) {
        Utils.openScreen("screens/View/Docent/OverzichtLes.fxml", "Langdurig afmelden");
    }

    public void toonAfwezigheid(ActionEvent actionEvent) {
        Utils.openScreen("screens/View/Docent/AfwezigheidTonenDocent.fxml", "Afwezigheid");
    }

    public void toonAanwezigheid(ActionEvent actionEvent) {
        Utils.openScreen("screens/View/Docent/AanwezigMeldenDocent.fxml", "Langdurig afmelden");
    }

    public void refresh() {
        lesBox.setItems(null);
        Docent user = (Docent) Main.getCurrentUser();
        ObservableList<Les> shownLessons = FXCollections.observableArrayList();
        if (verleden.isSelected()) {
            for (Les les: user.getLessons()) {
                shownLessons.add(les);
            }
        } else {
            for (Les les: user.getLessons()) {
                if (les.getEnd().isAfter(LocalDateTime.now())) {
                    shownLessons.add(les);
                }
            }
        }

        lesBox.setItems(shownLessons);
        Comparator<Les> sorter = Comparator.comparing(Les::getStart);
        lesBox.getItems().sort(sorter);

        lesBox.getSelectionModel().selectFirst();
        setCurrentLesson((Les) lesBox.getValue());
    }

    public static Les getCurrentLesson() {
        return currentLesson;
    }

    public static void setCurrentLesson(Les Lesson) {
        currentLesson = Lesson;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        Docent user = (Docent) Main.getCurrentUser();
        refresh();

        // formal greeting towards user
        welcomeUser.setText(welcomeUser.getText() + user.getName());
        todayDate.setText(todayDate.getText() + LocalDate.now().format(DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG)));
    }
}
