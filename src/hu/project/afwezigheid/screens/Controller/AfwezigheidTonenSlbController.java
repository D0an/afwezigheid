package hu.project.afwezigheid.screens.Controller;

import hu.project.afwezigheid.Main;
import hu.project.afwezigheid.screens.Model.Leerling;
import hu.project.afwezigheid.screens.Model.Les;
import hu.project.afwezigheid.screens.Model.Slber;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.net.URL;
import java.util.Comparator;
import java.util.ResourceBundle;

// Laat de studenten zien die onder de slb'er vallen en afwezig zijn
public class AfwezigheidTonenSlbController implements Initializable {
    public ListView studentList;
    private static Leerling selectedPupil;
    // Studentenlijst word getoond: naam + klas + aantal afwezige lessen / uren

    public void terug(ActionEvent actionEvent) {
        Utils.closeScreen();
    }

    public void sortHigh(ActionEvent actionEvent) {
        Comparator<Leerling> sorter = Comparator.comparingInt(Leerling::getTotalAbsents);
        studentList.getItems().sort(sorter.reversed());
    }

    public void sortLow(ActionEvent actionEvent) {
        Comparator<Leerling> sorter = Comparator.comparingInt(Leerling::getTotalAbsents);
        studentList.getItems().sort(sorter);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        Slber slbUser = (Slber) Main.getCurrentUser();
        ObservableList<Leerling> pupils = FXCollections.observableArrayList();
        for (Leerling pupil: slbUser.getStudents()) {
            pupils.add(pupil);
        }
        studentList.setItems(pupils);
        sortHigh(null);
    }

    public static void setPupil(Leerling pupil) {
        selectedPupil = pupil;
    }

    public static Leerling getSelectedPupil(){
        return selectedPupil;
    }

    public void pupilMouseReleased(MouseEvent mouseEvent) {
        if (mouseEvent.getClickCount() >= 2) {
            System.out.println("released on a pupil");
            Leerling pupil = (Leerling) studentList.getSelectionModel().getSelectedItem();
            setPupil(pupil);
            Utils.openScreen("screens/View/Slber/LeerlingProfileSlb.fxml", getSelectedPupil().getName());
        }
    }
}
