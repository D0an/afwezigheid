package hu.project.afwezigheid.screens.Controller;

import hu.project.afwezigheid.Main;
import hu.project.afwezigheid.screens.Model.Leerling;
import hu.project.afwezigheid.screens.Model.Les;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ResourceBundle;

public class HomescreenLeerlingController implements Initializable {

    public ListView lessenList;
    public Label welcomeMessage;
    public Label todayDate;

    // Loguit button voor de leerling
    public void logUit(ActionEvent actionEvent) {
        Utils.closeScreen();
    }

    // toon het afmeld scherm voor de student
    public void toonAfmelden(ActionEvent actionEvent) {
        Utils.openScreen("screens/View/Leerling/AfmeldschermLeerling.fxml", "Afmelden");
    }

    // toon een scherm waar de student zich kan aanmelden
    public void toonAanwezig(ActionEvent actionEvent) {
    }

    public void toonLangAfmelden(ActionEvent actionEvent) {
        Utils.openScreen("screens/View/Leerling/LangdurigAfmelden.fxml", "Langdurig afmelden");
    }

    // toon alle afwezigheden van een leerling
    public void toonOverzicht(ActionEvent actionEvent) {
        Utils.openScreen("screens/View/Leerling/AfwezigheidOverzichtLeerling.fxml", "Overzicht");
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        Leerling llrUser = (Leerling) Main.getCurrentUser();
        ObservableList<Les> shownLessons = FXCollections.observableArrayList();
        for (Les les: llrUser.getMyClass().getLessons()) {
            if (les.getStart().toLocalDate().isEqual(LocalDate.now())) {
                shownLessons.add(les);
            }
        }

        lessenList.setItems(shownLessons);

        welcomeMessage.setText(welcomeMessage.getText() + llrUser.getName());
        todayDate.setText(todayDate.getText() + LocalDate.now().format(DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG)));
    }


}
