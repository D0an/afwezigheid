package hu.project.afwezigheid.screens.Controller;

import hu.project.afwezigheid.screens.Model.Klas;
import hu.project.afwezigheid.screens.Model.Leerling;
import hu.project.afwezigheid.screens.Model.Les;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class OverzichtLesController implements Initializable {
    public ListView studentenLijst;

    public void backToMenu(ActionEvent actionEvent) {
        Utils.closeScreen();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        Les lesson = HomescreenDocentController.getCurrentLesson();
        Klas currentClass = lesson.getClassGivingTo();
        ObservableList<String> pupilsString = FXCollections.observableArrayList();
        for (Leerling pupil: currentClass.getStudents()
             ) {
            if (lesson.getAttendants().contains(pupil)) {
                pupilsString.add(pupil.toString() + " present");
            } else if (lesson.getAbsentees().contains(pupil)) {
                pupilsString.add(pupil.toString() + " afwezig");
            } else {
                pupilsString.add(pupil.toString() + " onbekend");
            }
        }
        studentenLijst.setItems(pupilsString);
    }
}
