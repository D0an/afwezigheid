package hu.project.afwezigheid.screens.Controller;

import hu.project.afwezigheid.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ResourceBundle;

public class HomescreenSlbController implements Initializable {
    public Label dateMessage;
    public Label welcomeMessage;

    public void logUit(ActionEvent actionEvent) {
        Utils.closeScreen();
    }

    public void toonOverzichtStudent(ActionEvent actionEvent) {
        Utils.openScreen("screens/View/Slber/StudentOverzichtSlb.fxml", "Overzicht");
    }

    public void toonAanwezigheidOverzicht(ActionEvent actionEvent) {
        Utils.openScreen("screens/View/Slber/AanwezigheidTonenSlb.fxml", "Aanwezigheid");
    }

    public void toonAfwezigheidOverzicht(ActionEvent actionEvent) {
        Utils.openScreen("screens/View/Slber/AfwezigheidTonenSlb.fxml", "Afwezigheid");
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        welcomeMessage.setText(welcomeMessage.getText() + Main.getCurrentUser().getName());
        dateMessage.setText(dateMessage.getText() + LocalDate.now().format(DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG)));
    }
}
