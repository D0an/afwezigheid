package hu.project.afwezigheid.screens.Controller;

import hu.project.afwezigheid.screens.Model.Klas;
import hu.project.afwezigheid.screens.Model.Leerling;
import hu.project.afwezigheid.screens.Model.Les;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class AanwezigMeldenDocentController implements Initializable {
    public ListView studentList;
    public RadioButton presentMeldenRadio;
    public RadioButton meldAfwezigRadio;
    private boolean radioRefresh = true;

    public void terug(ActionEvent actionEvent) {
        Utils.closeScreen();
    }

    public void meldPresentActive(ActionEvent actionEvent) {
        presentMeldenRadio.setSelected(true);
        meldAfwezigRadio.setSelected(false);
    }

    public void meldAfwezigActive(ActionEvent actionEvent) {
        presentMeldenRadio.setSelected(false);
        meldAfwezigRadio.setSelected(true);
    }

    public void pupilMouseReleased(MouseEvent mouseEvent) {
        if (mouseEvent.getClickCount() >= 2) {
            System.out.println("released on a pupil");
            String pupilString = (String) studentList.getSelectionModel().getSelectedItem();

            Les lesson = HomescreenDocentController.getCurrentLesson();
            Leerling pupil = null;
            for (Leerling pupilInClass: lesson.getClassGivingTo().getStudents()) {
                if (pupilString.contains(pupilInClass.toString())){
                    pupil = pupilInClass;
                }
            }
            if (presentMeldenRadio.isSelected()) {
                lesson.presentAnyway(pupil);
            } else {
                lesson.AbsentAnyway(pupil);
            }

            initialize(null, null);
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        if (radioRefresh) {
            presentMeldenRadio.setSelected(true);
            meldAfwezigRadio.setSelected(false);
            radioRefresh = false;
        }


        Les lesson = HomescreenDocentController.getCurrentLesson();
        Klas currentClass = lesson.getClassGivingTo();
        ObservableList<String> pupilsString = FXCollections.observableArrayList();
        for (Leerling pupil: currentClass.getStudents()
        ) {
            if (lesson.getAttendants().contains(pupil)) {
                pupilsString.add(pupil.toString() + " present");
            } else if (lesson.getAbsentees().contains(pupil)) {
                pupilsString.add(pupil.toString() + " afwezig");
            } else {
                pupilsString.add(pupil.toString() + " onbekend");
            }
        }
        studentList.setItems(pupilsString);
    }
}
