package hu.project.afwezigheid.screens.Controller;

import hu.project.afwezigheid.Main;
import hu.project.afwezigheid.screens.Model.Leerling;
import hu.project.afwezigheid.screens.Model.Slber;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class StudentOverzichtSlbController implements Initializable {

    public ListView studentenLijst;

    public void studentVerwijderen(ActionEvent actionEvent) {
        Slber slbUser = (Slber) Main.getCurrentUser();
        Leerling deleteUser = (Leerling) studentenLijst.getSelectionModel().getSelectedItem();
        slbUser.removeStudent(deleteUser);
        initialize(null, null);
    }

    public void terug(ActionEvent actionEvent) {
        Utils.closeScreen();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        Slber slbUser = (Slber) Main.getCurrentUser();
        ObservableList<Leerling> pupils = FXCollections.observableArrayList();
        for (Leerling pupil: slbUser.getStudents()) {
            pupils.add(pupil);
        }
        studentenLijst.setItems(pupils);
    }
}
