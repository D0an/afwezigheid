package hu.project.afwezigheid.screens.Controller;

import hu.project.afwezigheid.screens.Model.*;
import hu.project.afwezigheid.screens.Controller.HomescreenDocentController.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ListView;
import javafx.stage.Stage;

import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.ResourceBundle;


public class AfwezigheidTonenDocentController implements Initializable {
    public ListView studentList;
    private LocalDate datum = LocalDate.now();

    ObservableList absents = FXCollections.observableArrayList();
    Les lesson = HomescreenDocentController.getCurrentLesson();
    Klas currentClass = lesson.getClassGivingTo();

    public void absentsAdd() {

        for (Leerling students : currentClass.getStudents()) {
            for (Afwezigheid absence : students.getMyAbsents()) {
                if (absence instanceof DatumsAfwezigheid) {
                    if (((((DatumsAfwezigheid) absence).getBeginDate().isBefore(lesson.getStart().toLocalDate()) || ((DatumsAfwezigheid) absence).getBeginDate().isEqual(lesson.getStart().toLocalDate()))
                            && ((DatumsAfwezigheid) absence).getEndDate().isAfter(lesson.getStart().toLocalDate()) || ((DatumsAfwezigheid) absence).getEndDate().isEqual(lesson.getStart().toLocalDate()))) {
                        absents.add(absence);
                    }
                } else {
                    if (((LesAfwezigheid) absence).getLesson().equals(this.lesson)) {
                        absents.add(absence);
                    }
                }
            }
        }
    }

    public void terug(ActionEvent actionEvent) {
        Utils.closeScreen();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        absentsAdd();
        studentList.setItems(absents);
        System.out.println(absents);
    }
}
