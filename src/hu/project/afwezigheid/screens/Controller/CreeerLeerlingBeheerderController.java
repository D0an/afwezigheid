package hu.project.afwezigheid.screens.Controller;

import hu.project.afwezigheid.Main;
import hu.project.afwezigheid.screens.Model.Beheerder;
import hu.project.afwezigheid.screens.Model.Klas;
import hu.project.afwezigheid.screens.Model.Leerling;
import hu.project.afwezigheid.screens.Model.Slber;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.util.List;

public class CreeerLeerlingBeheerderController {
    public TextField voornaamField;
    public TextField tussenvoegselField;
    public TextField achternaamField;
    public TextField emailField;
    public TextField wachtwoordField;
    public ComboBox<Slber> slbBox;
    public ComboBox<Klas> klasBox;
    private Alert alert = new Alert(Alert.AlertType.WARNING);

    public void initialize() {
        List<Slber> allSlbers = Slber.getAllSlber();
        slbBox.setItems(FXCollections.observableList(allSlbers));
        List<Klas> allClasses = Klas.getAllClasses();
        klasBox.setItems(FXCollections.observableList(allClasses));
    }

    public void createLeerling(ActionEvent actionEvent) {
        if (!voornaamField.equals("")
                && !achternaamField.getText().isBlank()
                && !emailField.getText().isBlank()
                && !wachtwoordField.getText().isBlank()
                && slbBox.getValue() != null
                && klasBox.getValue() != null) {
            Leerling student;
            if (!tussenvoegselField.getText().isBlank()) {
                student = new Leerling(voornaamField.getText() + " " + tussenvoegselField.getText() + " " + achternaamField.getText(),
                        emailField.getText(), wachtwoordField.getText(), slbBox.getValue());
                klasBox.getValue().addStudent(student);
            } else {
                student = new Leerling(voornaamField.getText() + " " + achternaamField.getText(),
                        emailField.getText(), wachtwoordField.getText(), slbBox.getValue());
                klasBox.getValue().addStudent(student);
            }
            Beheerder.addStudent(student);
            System.out.println(Leerling.getAllStudents());

            Stage.getWindows().get(Stage.getWindows().size() - 1).hide();
        } else {
            alert.setTitle("Waarschuwing");
            alert.setHeaderText("Let op.");
            alert.setContentText("U moet alle velden invullen voordat u een student kunt aanmaken.");
            alert.show();
        }

        /*
        TO DO:
        Kan alleen een leerling aanmaken als er al een SLBer bestaat! else: foutmelding
         */
    }

    public void terug(ActionEvent actionEvent) {
        Utils.closeScreen();
    }
}
