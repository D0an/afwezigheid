package hu.project.afwezigheid.database;

import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.ReplaceOptions;
import hu.project.afwezigheid.screens.Model.*;
import org.bson.Document;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class DatabaseManager {

    private MongoClient mongoClient;
    private MongoDatabase database;
    private MongoCollection<Document> beheerderCollection;
    private MongoCollection<Document> slberCollection;
    private MongoCollection<Document> leerlingCollection;
    private MongoCollection<Document> docentCollection;
    private MongoCollection<Document> lessenCollection;

    public DatabaseManager() {
        String userPart = "";
        if (Credentials.isAuth()) {
            userPart = Credentials.getUsername() + ":" + Credentials.getPassword() + "@";
        }
        mongoClient = new MongoClient(new MongoClientURI("mongodb://" + userPart + Credentials.getHost() + ":" + Credentials.getPort() + (Credentials.isAuth() ? "/school" : "")));
        try {
            mongoClient.listDatabases().forEach((Block<? super Document>) db -> db.toJson()); // lijn om te testen of de database ingeladen is.
            System.out.println("[Afwezigheid] Database ingeladen.");

            database = mongoClient.getDatabase("school");
            beheerderCollection = database.getCollection("Beheerders");
            slberCollection = database.getCollection("Slbers");
            docentCollection = database.getCollection("Docenten");
            leerlingCollection = database.getCollection("Leerlingen");

            lessenCollection = database.getCollection("Lessen");
        } catch (Exception ex) {
            System.out.println("[Afwezigheid] [ERROR] Failed to initialize backend.");
            System.out.println("[Afwezigheid] [ERROR] " + ex.getClass().getSimpleName() + " - " + ex.getMessage());
            System.exit(-1);
        }
        reloadAllData();
    }

    public void reloadAllData(){
        Beheerder.getAllAdmins().clear();
        loadAllAdmins().forEach(admin -> Beheerder.getAllAdmins().add(admin));

        Slber.getAllSlber().clear();
        loadAllSlbers().forEach(slber -> Slber.getAllSlber().add(slber));

        Docent.getAllTeachers().clear();
        loadAllTeachers().forEach(teacher -> Docent.getAllTeachers().add(teacher));

        loadAllLessons();

        Leerling.getAllStudents().clear();
        loadAllStudents().forEach(student -> Leerling.getAllStudents().add(student));

    }

    //TODO: Functies voor inladen / opslaan gegevens

    public List<Beheerder> loadAllAdmins(){
        List<Beheerder> admins = new ArrayList<>();
        for (Document document : beheerderCollection.find()) {
            admins.add(new Beheerder(document.getString("name"), document.getString("email"), document.getString("password")));
        }
        return admins;
    }

    public List<Slber> loadAllSlbers(){
        List<Slber> slbers = new ArrayList<>();
        for (Document document : slberCollection.find()) {
            slbers.add(new Slber(document.getString("name"), document.getString("email"), document.getString("password")));
        }
        return slbers;
    }

    public List<Leerling> loadAllStudents(){
        List<Leerling> students = new ArrayList<>();
        for (Document document : leerlingCollection.find()) {
            Slber mentor = (Slber) Beheerder.getPersonFromEmail(document.getString("mentor"));

            String klasnr = document.getString("klas");
            Klas klas = Klas.getClassFromClassNumber(klasnr);
            if(klas == null) klas = new Klas(klasnr);

            Leerling leerling = new Leerling(document.getString("name"), document.getString("email"), document.getString("password"), mentor);
            klas.addStudent(leerling);
            students.add(leerling);

            List<Document> afwezigheden = document.getList("afwezigheden", Document.class);
            for (Document afwezigheid : afwezigheden) {
                if("datum".equals(afwezigheid.getString("type"))) {
                    LocalDate beginDatum = LocalDate.ofInstant(afwezigheid.getDate("beginDate").toInstant(), ZoneId.systemDefault());
                    LocalDate eindDatum = LocalDate.ofInstant(afwezigheid.getDate("endDate").toInstant(), ZoneId.systemDefault());
                    new DatumsAfwezigheid(leerling, afwezigheid.getString("reason"), beginDatum, eindDatum);
                } else {
                    System.out.println(klas.getLesson(afwezigheid.getString("lesson")));
                    new LesAfwezigheid(leerling, klas.getLesson(afwezigheid.getString("lesson")), afwezigheid.getString("reason"));
                }
            }
        }
        return students;
    }

    public List<Docent> loadAllTeachers(){
        List<Docent> teachers = new ArrayList<>();
        for (Document document : docentCollection.find()) {
            teachers.add(new Docent(document.getString("name"), document.getString("email"), document.getString("password")));
        }
        return teachers;
    }

    public void loadAllLessons() {
        for (Document document : lessenCollection.find()) {
            String klasnr = document.getString("klas");
            Klas klas = Klas.getClassFromClassNumber(klasnr);
            if(klas == null) klas = new Klas(klasnr);

            LocalDateTime start = LocalDateTime.ofInstant(document.getDate("start").toInstant(), ZoneId.systemDefault());
            LocalDateTime end = LocalDateTime.ofInstant(document.getDate("end").toInstant(), ZoneId.systemDefault());
            System.out.println(start.toString());
            Docent teacher = (Docent) Beheerder.getPersonFromEmail(document.getString("docent"));

            UUID uniqueId = UUID.fromString(document.getString("uuid"));

            new Les(uniqueId, start, end, klas, teacher);
        }
    }

    public void saveAdmin(Beheerder admin) {
        Document document = new Document()
                .append("name", admin.getName())
                .append("email", admin.getEmail())
                .append("password", admin.getPassword());

        beheerderCollection.replaceOne(Filters.eq("email", admin.getEmail()), document, new ReplaceOptions().upsert(true));
    }

    public void saveSlber(Slber slber) {
        Document document = new Document()
                .append("name", slber.getName())
                .append("email", slber.getEmail())
                .append("password", slber.getPassword());

        slberCollection.replaceOne(Filters.eq("email", slber.getEmail()), document, new ReplaceOptions().upsert(true));
    }

    public void saveTeacher(Docent teacher) {
        Document document = new Document()
                .append("name", teacher.getName())
                .append("email", teacher.getEmail())
                .append("password", teacher.getPassword());

        docentCollection.replaceOne(Filters.eq("email", teacher.getEmail()), document, new ReplaceOptions().upsert(true));
    }

    public Date convertToDateViaInstant(LocalDate dateToConvert) {
        return Date.from(dateToConvert.atStartOfDay()
                .atZone(ZoneId.systemDefault())
                .toInstant());
    }


    public void saveStudent(Leerling student) {
        List<Document> afwezigheden = new ArrayList<>();

        student.getMyAbsents().forEach(afwezigheid -> {

            if (afwezigheid instanceof DatumsAfwezigheid) {
                DatumsAfwezigheid dateAbsent = (DatumsAfwezigheid) afwezigheid;
                afwezigheden.add(new Document()
                        .append("type", "datum")
                        .append("beginDate", convertToDateViaInstant(dateAbsent.getBeginDate()))
                        .append("endDate", convertToDateViaInstant(dateAbsent.getEndDate()))
                        .append("reason", dateAbsent.getReason()));
            } else {
                LesAfwezigheid lessonAbsent = (LesAfwezigheid) afwezigheid;
                afwezigheden.add(new Document()
                        .append("type", "les")
                        .append("lesson", lessonAbsent.getLesson().getUniqueId().toString())
                        .append("reason", lessonAbsent.getReason()));
            }

        });

        Document document = new Document()
                .append("name", student.getName())
                .append("email", student.getEmail())
                .append("password", student.getPassword())
                .append("mentor", student.getMentor().getEmail())
                .append("klas", student.getMyClass().toString())
                .append("afwezigheden", afwezigheden);

        leerlingCollection.replaceOne(Filters.eq("email", student.getEmail()), document, new ReplaceOptions().upsert(true));

    }
}
