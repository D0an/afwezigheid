package hu.project.afwezigheid;

import hu.project.afwezigheid.database.DatabaseManager;
import hu.project.afwezigheid.screens.Model.Persoon;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    private static DatabaseManager databaseManager;
    private static Persoon currentUser;

    @Override
    public void start(Stage primaryStage) throws Exception{
//        Parent root = FXMLLoader.load(getClass().getResource("screens/View/Login.fxml"));
//        primaryStage.setTitle("Afwezigheid");
//        primaryStage.setScene(new Scene(root, 550, 400));
//        primaryStage.setResizable(false);
//        primaryStage.show();
        String fxmlPagina = "screens/View/Login.fxml";
        FXMLLoader loader = new FXMLLoader(getClass().getResource(fxmlPagina));
        Parent root = loader.load();

        primaryStage.setTitle("Login");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    public static void main(String[] args) {
        databaseManager = new DatabaseManager();
        launch(args);
    }

    public static DatabaseManager getDatabaseManager() {
        return databaseManager;
    }

    public static void setCurrentUser(Persoon user){
        currentUser = user;
    }

    public static Persoon getCurrentUser() {
        return currentUser;
    }

}
